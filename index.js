import { AppRegistry } from 'react-native'

import { name as AppName } from './app.json'

import * as Contexts from './src/contexts'
import * as Navigation from './src/navigation'


AppRegistry.registerComponent(AppName, () => (
  <Contexts.Collection>
    <Navigation.App />
  </Contexts.Collection>
))