import { ScaledSize } from "react-native"

export interface IDimensions {
  screen: ScaledSize
  window: ScaledSize
}


export type TFontConstructor = {
  (
    type?: TFontWeight,
    size?: number,
    color?: string
  ): string
}

type TFontWeight =
  'Light' |
  'Regular' |
  'Bold'


export type TFlexConstuctor = {
  (
    direction?: TFlexDirections,
    justifyContent?: TFlexJustify,
    alignItems?: TFlexAlign,
    wrap?: TFlexWrap
  ): string
}

type TFlexDirections =
  'row' |
  'row-reverse' |
  'column' |
  'column-reverse'

type TFlexJustify =
  'flex-start' |
  'flex-end' |
  'center' |
  'space-between' |
  'space-around' |
  'space-evenly'

type TFlexAlign = TFlexJustify |
  'stretch' |
  'baseline'

type TFlexWrap =
  'nowrap' |
  'wrap'


export interface ISides {
  _one: (pxTo: number) => boolean
  _multi: (from: number, to: number) => boolean
}

export type TDeviceSizeProps = {
  s: boolean,
  m: boolean,
  select: IDeviceSizeSelectProps
}

interface IDeviceSizeSelectProps {
  (size: {
    s?: string | number,
    m?: string | number,
    _default?: string | number,
  }): any
}