import { Dimensions } from 'react-native'
import {
  ISides,
  IDimensions,
  TFlexConstuctor,
  TFontConstructor,
  TDeviceSizeProps,
} from './types'


export const DIMENSIONS: IDimensions = {
  screen: Dimensions.get('screen'),
  window: Dimensions.get('window')
}

export const FontFamily: string = 'Arial'

export const FONT: TFontConstructor = (
  type = 'Regular',
  size = 18,
  color = '#000'
) => `
  color: ${color};
  font-size: ${size};
  font-weight: ${type};
`

export const FLEX: TFlexConstuctor = (
  direction = 'column',
  justifyContent = 'center',
  alignItems = 'center',
  wrap = 'nowrap'
) => `
  flex-direction: ${direction};
  justify-content: ${justifyContent};
  align-items: ${alignItems};
  flex-wrap: ${wrap};
`


const Sides: ISides = {
  _one: pxTo => Math.round(DIMENSIONS.screen.width) <= pxTo,
  _multi: (from, to) => {
    const height = Math.round(DIMENSIONS.screen.height)
    return ((height >= from) && (height <= to))
  }
}

const isDeviceS: boolean = (Sides._one(360) && Sides._multi(0, 664)) || Sides._one(320) ? true : false

const isDeviceM: boolean = Sides._multi(667, 736) ? true : false

export const DeviceSize: TDeviceSizeProps = {
  s: isDeviceS,
  m: isDeviceM,
  select: ({
    s = null,
    m = null,
    _default = null
  }) => {
    if (isDeviceS && s) return s
    else if (isDeviceM && m) return m
    else return _default
  }
}
