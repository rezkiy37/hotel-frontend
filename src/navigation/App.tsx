import React, { useContext } from 'react'
import { NavigationContainer } from '@react-navigation/native'

import * as Types from '../types'
import * as Contexts from '../contexts'
import * as Screens from '../components/Screen'

const App: Types.TNavElement = ({ navigation }) => {

  const {
    isReady, isAuthenticated
  } = useContext(Contexts.AuthContext)

  if (!isReady) {
    return <Screens.Loading />
  }

  if (isAuthenticated) {
    return (
      <NavigationContainer>

      </NavigationContainer>
    )
  }

  return (
    <NavigationContainer>

    </NavigationContainer>
  )
}

export default App