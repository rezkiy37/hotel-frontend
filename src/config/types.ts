export interface IFetus {
  id: string
  title: string
  path: string
}

export interface IRudiment extends IFetus {
  type: 'stack' | 'tabs' | 'drawer'
  child: string
}

export enum ENavDrawerPaths {
  GeneralBranch = 'DrawerGeneralStack',
  ProfileBranch = 'DrawerProfileStack',
  SettingsBranch = 'DrawerSettingsStack'
}

export enum ENavDrawerChildren {
  GeneralBranch = 'DrawerGeneralBranch',
  ProfileBranch = 'DrawerProfileBranch',
  SettingsBranch = 'DrawerSettingsBranch'
}

export interface INavTree {

}