import { ReactElement } from 'react'
import { StackNavigationProp } from '@react-navigation/stack'
import { NavigationProp, Route } from '@react-navigation/native'

export type TNavElement = {
  (
    props: {
      navigation: NavigationProp<any>
    }
  ): ReactElement
}

export type TStackNavElement = {
  (
    props: {
      navigation: StackNavigationProp<any>,
      route: Route<any>
    }
  ): ReactElement
}