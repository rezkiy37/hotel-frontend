export interface IAuthContext {
  token: TToken | null
  userID: TUserID | null
  isReady: boolean
  isAuthenticated: boolean
  login: TLogin
  logout: TLogout
}

export enum EVars {
  USER_DATA = 'USER_DATA'
}

export type TToken = string
export type TUserID = string

export interface IUserData {
  token: TToken
  userID: TUserID
}

export interface IAuthContextCallbacks {
  LOGIN: TLogin
  LOGOUT: TLogout
  GET_USER: TGET_USER
}

type TLogin = {
  (
    token: TToken, userID: TUserID
  ): Promise<void>
}
type TLogout = {
  (): Promise<void>
}

type TGET_USER = {
  (): Promise<void>
}

