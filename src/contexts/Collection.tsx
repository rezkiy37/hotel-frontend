import React from 'react'

import * as Contexts from '.'


const Collection: React.FC = ({ children }) => {

  return (
    <Contexts.AuthContextProvider>
      {children}
    </Contexts.AuthContextProvider>
  )
}

export default Collection