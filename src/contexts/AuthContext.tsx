import React, { createContext, useEffect, useState } from 'react'
import { AsyncStorage } from 'react-native'

import {
  EVars,
  TToken,
  TUserID,
  IUserData,
  IAuthContext,
  IAuthContextCallbacks,
} from './types'


const AuthContext = createContext<IAuthContext>({
  token: null,
  userID: null,
  isReady: false,
  isAuthenticated: false,
  login: async () => { },
  logout: async () => { }
})

export default AuthContext


export const AuthContextProvider: React.FC = ({ children }) => {

  const [token, setToken] = useState<TToken | null>(null)
  const [userID, setUserID] = useState<TUserID | null>(null)
  const [isReady, setReady] = useState<boolean>(false)
  const [isAuthenticated, setAuthenticated] = useState<boolean>(false)

  const Callbacks: IAuthContextCallbacks = {
    LOGIN: async (token: string, userID: string) => {
      try {
        console.log('LOGIN')

        setToken(token)
        setUserID(userID)
        setAuthenticated(true)

        await AsyncStorage.setItem(EVars.USER_DATA, JSON.stringify({ token, userID }))

        setReady(true)
      } catch (e) {
        console.log(e)
      }
    },
    LOGOUT: async () => {
      try {
        setAuthenticated(false)

        await AsyncStorage.removeItem(EVars.USER_DATA)
      } catch (e) {
        console.log(e)
      }
    },
    GET_USER: async () => {
      try {
        console.log('GET_USER')

        setReady(false)

        const response: string | null = await AsyncStorage.getItem(EVars.USER_DATA)

        if (response) {
          const data: IUserData = JSON.parse(response)

          if (data.token && data.userID) Callbacks.LOGIN(data.token, data.userID)
        } else {
          console.log('There is not user')
          setReady(true)
        }
      } catch (e) {
        console.log(e)
      }
    }
  }

  useEffect(() => {
    console.log('useEffect')

    Callbacks.GET_USER()
  }, [Callbacks.LOGIN])

  return (
    <AuthContext.Provider value={{
      token, userID, isReady, isAuthenticated,
      login: Callbacks.LOGIN,
      logout: Callbacks.LOGOUT
    }}>
      {children}
    </AuthContext.Provider>
  )
}