import React from 'react'
import { ActivityIndicator } from 'react-native'

const Loading: React.FC = () => {

  return (
    <ActivityIndicator size="large" color="#00ff00" />
  )
}

export default Loading