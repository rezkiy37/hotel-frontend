import { ReactElement } from 'react'
import { ViewStyle } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'


export interface IContainerProps {
  children: ReactElement
  scrollable?: boolean
  containerStyle?: ViewStyle
  scrollContainerStyle?: ViewStyle
  scrollContentViewStyle?: ViewStyle
}

export interface IHeaderProps {
  title: string
  navigation: StackNavigationProp<any>
}