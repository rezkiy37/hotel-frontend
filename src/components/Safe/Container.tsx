import React from 'react'
import styled from 'styled-components/native'

import { IContainerProps } from './types'


const Container: React.FC<IContainerProps> = ({
  children,
  scrollable = false,
  containerStyle = {},
  scrollContainerStyle = {},
  scrollContentViewStyle = {}
}) => {

  return (
    <SafeView
      style={containerStyle}
    >
      {scrollable ? (
        <SafeScrollView
          style={scrollContainerStyle}
          contentContainerStyle={[{
            paddingBottom: 0 // was 20
          }, scrollContentViewStyle]}
          showsVerticalScrollIndicator={false}
        >
          {children}
        </SafeScrollView>
      ) : children}
    </SafeView>
  )
}

const SafeView = styled.SafeAreaView`
  flex: 1;
`

const SafeScrollView = styled.ScrollView`
  width: 100%;
`

export default Container