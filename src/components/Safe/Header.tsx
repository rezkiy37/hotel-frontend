import React from 'react'
import { StyleSheet } from 'react-native'
import styled from 'styled-components/native'
import { Header as NativeHeader } from 'react-native-elements'

import { IHeaderProps } from './types'


const Header: React.FC<IHeaderProps> = ({
  title,
  navigation
}) => {

  const Renders = {
    Center: () => {
      return (
        <Title>
          {title}
        </Title>
      )
    }
  }

  return (
    <NativeHeader
      containerStyle={header}
      centerComponent={Renders.Center()}
    />
  )
}

const { header } = StyleSheet.create({
  header: {
    backgroundColor: 'white'
  }
})

const Title = styled.Text``


export default Header